const express = require('express');
const userController = require('../controllers/user.controller');

const router = express.Router();

router.post("/", async (req, res, next) => await userController.createUser(req, res, next));

router.put("/", async (req, res, next) => await userController.updateUser(req, res, next));

router.delete("/:id", async (req, res, next) => await userController.deleteUser(req, res, next));

router.get("/:id", async (req, res, next) => await userController.getUserById(req, res, next));

router.get("/chat/:chatId", async (req, res, next) => await userController.getUserByChatId(req, res, next));

router.get("/active/all", async (req, res, next) => await userController.getActiveUsers(req, res, next));

router.get("/inactive/all", async (req, res, next) => await userController.getInActiveUsers(req, res, next));

router.post("/send-msg", async (req, res, next) => await userController.sendMessage(req, res, next));

router.post("/shopify-order-info", async (req, res, next) => await userController.getOrderDetails(req, res, next));

router.get("/get-user/count", async (req, res, next) => await userController.getUserCount(req, res, next));



module.exports = router;