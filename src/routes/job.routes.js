const express = require('express');
const jobController = require('../controllers/job.controller');

const router = express.Router();

router.post("/",  async (req, res, next) => await jobController.createJob(req, res, next));

router.put("/:jobId",  async (req, res, next) => await jobController.updateJob(req, res, next));

router.get("/:jobId",  async (req, res, next) => await jobController.getJobById(req, res, next));

router.delete("/:jobId",  async (req, res, next) => await jobController.deleteJobById(req, res, next));

module.exports = router;