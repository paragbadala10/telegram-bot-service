const express = require('express');
const addressController = require('../controllers/address.controller');

const router = express.Router();

router.post("/",  async (req, res, next) => await addressController.createAddress(req, res, next));

router.put("/:chatId",  async (req, res, next) => await addressController.updateAddress(req, res, next));

router.get("/:chatId",  async (req, res, next) => await addressController.getAddress(req, res, next));

router.delete("/:id",  async (req, res, next) => await addressController.deleteAddress(req, res, next));

module.exports = router;