const addressRepository = require("../repository/address.repository");

module.exports = {
  createAddress: async (req, res, next) => {
    const data = req.body;

    try {
      const address = await addressRepository.saveAddress(data);
      return res.status(200).json({
        address: address,
      });
    } catch (err) {
      next(err);
    }
  },

  updateAddress: async (req, res, next) => {
    const data = req.body;
    const { chatId } = req.params;

    try {
      const address = await addressRepository.updateAddress(data, chatId);
      return res.status(200).json({
        address: address,
      });
    } catch (err) {
      next(err);
    }
  },

  deleteAddress: async (req, res, next) => {
    const { id } = req.params;

    try {
      const data = await addressRepository.deleteAddress(id);
      return res.status(200).json({
        address: data,
      });
    } catch (err) {
      next(err);
    }
  },

  getAddress: async (req, res, next) => {
    const { chatId } = req.params;

    if (chatId === undefined) {
      return res.status(400).json({
        error: "Chat id is required",
        error_code: "BAD_REQUEST",
        code: 400,
      });
    }

    try {
      const data = await addressRepository.getAddressByChatId(chatId);

      return res.status(200).json({
        address: data,
      });
    } catch (err) {
      next(err);
    }
  },
};
