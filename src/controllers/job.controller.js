const { verifyJob } = require("../helper/job.helper");
const jobRepository = require("../repository/job.repository");

module.exports = {
  createJob: async (req, res, next) => {
    const data = req.body;

    const isValid = verifyJob(data);

    if (isValid !== true) {
      return res.status(isValid.code).json(isValid);
    }

    try {
      const job = await jobRepository.createJob(data);
      return res.status(200).json({
        job: job,
      });
    } catch (err) {
      next(err);
    }
  },

  updateJob: async (req, res, next) => {
    const data = req.body;
    const { jobId } = req.params;
    try {
      const job = await jobRepository.updateJob(data, jobId);
      return res.status(200).json({
        job: job,
      });
    } catch (err) {
      next(err);
    }
  },

  getJobById: async (req, res, next) => {
    const { jobId } = req.params;
    try {
      const job = await jobRepository.getJobById(jobId);
      return res.status(200).json({
        job: job,
      });
    } catch (err) {
      next(err);
    }
  },

  deleteJobById: async (req, res, next) => {
    const { jobId } = req.params;
    try {
      const job = await jobRepository.deleteJobById(jobId);
      return res.status(200).json({
        job: job,
      });
    } catch (err) {
      next(err);
    }
  }


};
