const { validateSendMessage } = require("../helper/user.helper");
const userHelper = require("../helper/user.helper");
const jobRepository = require("../repository/job.repository");
const userRepository = require("../repository/user.repository");
const { processSendMessages } = require("../utils/messenger.util");
const { default: axios } = require("axios");


const TOKEN = "667508284:AAGelU2rTNn3jbRcL6_Ednr_QhiHqf2S8r4";
const ADMIN_CHAT_ID = "224620384"

module.exports = {
  createUser: async (req, res, next) => {
    const data = req.body;

    const isValid = userHelper.verifyUser(data);

    if (isValid !== true) {
      return res.status(isValid.code).json(isValid);
    }

    try {
      const result = await userRepository.createUser(data);
      return res.status(200).json({
        user: result,
      });
    } catch (err) {
      next(err);
    }
  },

  updateUser: async (req, res, next) => {
    const data = req.body;

    const isValid = userHelper.verifyUser(data);

    if (isValid !== true) {
      return res.status(isValid.code).json(isValid);
    }

    try {
      const result = await userRepository.updateUser(data);
      return res.status(200).json({
        user: result,
      });
    } catch (err) {
      next(err);
    }
  },

  deleteUser: async (req, res, next) => {
    const { id } = req.params;

    if (!id) {
      return res.status(400).json({
        code: 400,
        error: "User id is required",
        error_code: "BAD_REQUEST",
      });
    }

    try {
      const result = await userRepository.deleteUser(id);
      return res.status(200).json({
        user: result,
      });
    } catch (err) {
      next(err);
    }
  },

  getUserById: async (req, res, next) => {
    const { id } = req.params;

    if (!id) {
      return res.status(400).json({
        code: 400,
        error: "User id is required",
        error_code: "BAD_REQUEST",
      });
    }

    try {
      const result = await userRepository.getUserById(id);
      return res.status(200).json({
        user: result,
      });
    } catch (err) {
      next(err);
    }
  },

  getUserByChatId: async (req, res, next) => {
    const { chatId } = req.params;

    if (!chatId) {
      return res.status(400).json({
        code: 400,
        error: "User id is required",
        error_code: "BAD_REQUEST",
      });
    }

    try {
      const result = await userRepository.getUserByChatId(chatId);
      return res.status(200).json({
        user: result,
      });
    } catch (err) {
      next(err);
    }
  },

  getActiveUsers: async (req, res, next) => {
    try {
      const users =
        await userRepository.getChatIdsByLastInteractionLessThan30Days();
      return res.status(200).json({
        user: users,
      });
    } catch (err) {
      next(err);
    }
  },

  getInActiveUsers: async (req, res, next) => {
    try {
      const users =
        await userRepository.getChatIdsByLastInteractionMoreThan30Days();
      return res.status(200).json({
        user: users,
      });
    } catch (err) {
      next(err);
    }
  },



  sendMessage: async (req, res, next) => {
    const data = req.body;
    const isValid = validateSendMessage(data);

    if (isValid !== true) {
      return res.status(isValid.code).json(isValid);
    }

    let ids = [];
    let activeIds = [];

    try {
      if (data.userType === "ACTIVE") {
        const activeUsers =
          await userRepository.getChatIdsByLastInteractionLessThan30Days();
        ids = activeUsers.map((e) => e.chatId);
        activeIds = ids.filter((e, i) => !ids.includes(e, i + 1));
      } else {
        const inActiveUsers =
          await userRepository.getChatIdsByLastInteractionMoreThan30Days();
        ids = inActiveUsers.map((e) => e.chatId);
        activeIds = ids.filter((e, i) => !ids.includes(e, i + 1));
      }

      const job = await jobRepository.createJob({
        status: "CREATED",
      });

      processSendMessages(job._id, activeIds, data.message, next);

      return res.status(200).json({
        job: job,
      });
    } catch (err) {
      next(err);
    }
  },

  getOrderDetails: async (req, res, next) => { 
    let orderInfo = req.body;
    let chatId;
    let googleMapUrl='';
    let orderItem='';
    let customerAddress = '';
    if(orderInfo.shipping_address.address1) {
      customerAddress += orderInfo.shipping_address.address1 + ', '
    }
    if(orderInfo.shipping_address.address2) {
      customerAddress += orderInfo.shipping_address.address2 + ', '
    }
    if(orderInfo.shipping_address.city) {
      customerAddress += orderInfo.shipping_address.city + ', '
    }
    if(orderInfo.shipping_address.province) {
      customerAddress += orderInfo.shipping_address.province + ', '
    }
    if(orderInfo.shipping_address.country) {
      customerAddress += orderInfo.shipping_address.country + ', '
    }
    if(orderInfo.shipping_address.phone) {
      customerAddress += orderInfo.shipping_address.phone
    }

    if(orderInfo && orderInfo.note_attributes && orderInfo.note_attributes.length && orderInfo.note_attributes[0].value) {
      chatId = orderInfo.note_attributes[0].value
    }

    if(orderInfo && orderInfo.note_attributes && orderInfo.note_attributes.length >= 1 && orderInfo.note_attributes[1].value) {
      googleMapUrl = orderInfo.note_attributes[1].value
    }

    if(orderInfo && orderInfo.line_items && orderInfo.line_items.length) {
      orderInfo.line_items.forEach((item, idx) => {
        orderItem += `${idx+1}) ${item.name} x ${item.quantity} - <b>${item.price}</b> \n`
      })
    }

    const message = `<b>Order Detail - #${orderInfo.order_number}</b>\n\n<b>Customer Name</b> -  ${orderInfo.customer.first_name} ${orderInfo.customer.last_name}\n\n<b>Customer Address</b> - ${customerAddress}\n\n<b>Location</b> - https://map.google.com/?q=${googleMapUrl}\n\n<b>Order Items</b>\n${orderItem}\nTotal Amount - <b>${orderInfo.total_price}</b>\n\n😋🍱🍛🍱🥗🍱🍲🍱🍲😍\n\nThank you for Ordering Food🤩`
    let encodedMsg = encodeURIComponent(message)
    try {
      await axios.get(`https://api.telegram.org/bot${TOKEN}/sendMessage?chat_id=${chatId}&&text=${encodedMsg}&parse_mode=html`)
      await axios.get(`https://api.telegram.org/bot${TOKEN}/sendMessage?chat_id=${ADMIN_CHAT_ID}&text=${encodedMsg}&parse_mode=html`)
      return res.status(200).json({
        Message: 'Msg sent',
      });
    } catch(err) {
      next(err);
    }
   },

   getUserCount: async (req, res, next) => {
    try {
      const data = await userRepository.getUserMessageRecord();
      return res.status(200).json(data);
    } catch (err) {
      next(err);
    }
   }
};
