module.exports = {

    verifyJob: (job) => {
       
        if (!job.status && job.status !== 0 && job.status !== false) {
            return {
                code: 400,
                error: "Job Status is required",
                error_code: "BAD_REQUEST"
            }
        }
        
        return true;
    },

};