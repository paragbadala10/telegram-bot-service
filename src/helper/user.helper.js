module.exports = {

    verifyUser: (user) => {
        if (!user.chatId && user.chatId !== 0 && user.chatId !== false) {
            return {
                code: 400,
                error: "Chat id is required",
                error_code: "BAD_REQUEST"
            }
        }

        if (!user.firstName && user.firstName !== 0 && user.firstName !== false) {
            return {
                code: 400,
                error: "First name is required",
                error_code: "BAD_REQUEST"
            }
        }

        // if (!user.lastName && user.lastName !== 0 && user.lastName !== false) {
        //     return {
        //         code: 400,
        //         error: "Last name is required",
        //         error_code: "BAD_REQUEST"
        //     }
        // }

        if (!user.last_interaction_with_bot && user.last_interaction_with_bot !== 0 && user.last_interaction_with_bot !== false) {
            return {
                code: 400,
                error: "Last bot interaction is required",
                error_code: "BAD_REQUEST"
            }
        }
        
        return true;
    },

    validateSendMessage: (data) => {
        if (!data.userType && data.userType !== 0 && data.userType !== false) {
            return {
                code: 400,
                error: "User type is required",
                error_code: "BAD_REQUEST"
            }
        }

        if (!data.message && data.message !== 0 && data.message !== false) {
            return {
                code: 400,
                error: "Message is required",
                error_code: "BAD_REQUEST"
            }
        }

        return true;

    }

};