const { Schema, default: mongoose } = require("mongoose");

const UserSchema = new Schema(
  {
    chatId: {
        type: String,
    },
    firstName: {
        type: String,
    },
    lastName: {
        type: String,
    },
    last_interaction_with_bot: {
        type: Date,
    }
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("users", UserSchema);

module.exports = User;
