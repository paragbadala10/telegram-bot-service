const { Schema, default: mongoose } = require("mongoose");

const AddressSchema = new Schema(
  {
    chatId: {
      type: String,
    },
    address: {
      type: Object,
    },
  },
  {
    timestamps: true,
  }
);

const Address = mongoose.model("addresses", AddressSchema);

module.exports = Address;
