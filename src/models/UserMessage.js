const { Schema, default: mongoose } = require("mongoose");

const UserMessageSchema = new Schema(
  {
    date: {
      type: String,
    },
    count: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

const UserMessage = mongoose.model("user-messages-count", UserMessageSchema);

module.exports = UserMessage;
