const { Schema, default: mongoose } = require("mongoose");

const JobSchema = new Schema(
  {
    reason: {
      type: String,
    },
    status: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const Job = mongoose.model("jobs", JobSchema);

module.exports = Job;
