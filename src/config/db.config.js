const mongoose = require("mongoose");
require("dotenv").config();

const dbConnection = async () => {
  await mongoose.connect(process.env.MONGODB_URI);
};

dbConnection().then(() => console.info("Connection established"));
dbConnection().catch((err) => console.error(err));

module.exports = {
  dbConnection,
};
