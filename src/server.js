const express = require("express");
const cors = require("cors");
require("dotenv").config();
const errorHandler = require("./middlewares/error.handler");
const userRoutes = require("./routes/user.routes");
const jobRoutes = require("./routes/job.routes");
const addressRoutes = require("./routes/address.routes");
const mongoose = require("mongoose");
require("./config/db.config").dbConnection();
const bot = require("./bot");
const app = express();

bot.launch();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get(process.env.CONTEXT_PATH + "/v1/health-check", (req, res) => {
  return res.status(200).json({
    message: "Healthy Server",
  });
});

app.use(process.env.CONTEXT_PATH + "/v1/user", userRoutes);
app.use(process.env.CONTEXT_PATH + "/v1/job", jobRoutes);
app.use(process.env.CONTEXT_PATH + "/v1/address", addressRoutes);

app.use((req, res, next) => {
  return res.status(404).json({ message: `Route not found` });
});

app.use(errorHandler);

app.listen(process.env.PORT, () => {
  console.log(`Server Up`);
});

// Close mongoose connection on crash
process.on("beforeExit", (code) => {
  mongoose.connection.close();
});

process.on("SIGTERM", (signal) => {
  mongoose.connection.close();
  process.exit(0);
});

process.on("uncaughtException", (err) => {
  mongoose.connection.close();
  process.exit(1);
});