// https://api.telegram.org/bot5368977819:AAFR7GjbCNIyZ0yvaMChhFU2Jb8y3ToZ2qw/sendMessage?chat_id=5090388344&&text=Hi%20from%20tele%20bot

const { default: axios } = require("axios");
const Job = require("../models/Job");
const jobRepository = require("../repository/job.repository");
const userRepository = require("../repository/user.repository");
const TOKEN = "667508284:AAGelU2rTNn3jbRcL6_Ednr_QhiHqf2S8r4";
const ADMIN_CHAT_ID = "224620384"

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

module.exports = {
  processSendMessages: async (jobId, chatIds, message, next) => {
    try {

      const chatIdsLength = chatIds.length;
      const dubaiTime = new Date(new Date - 1.5 * 60 * 60 * 1000).toLocaleString()
      let notificationSummarry = `<b>Notification Summary</b>\n\n<b>Date</b> - ${dubaiTime}\n\n<b>Total Message Sent to Customer</b> - ${chatIds.length}`

      notificationSummarry = encodeURIComponent(notificationSummarry)
      await axios.get(`https://api.telegram.org/bot${TOKEN}/sendMessage?chat_id=${ADMIN_CHAT_ID}&&text=${notificationSummarry}&parse_mode=html`)

      for (var i = 0; i < chatIdsLength; i++) {
        if (i % 28 === 0) {
          await sleep(3000);
        }
        let encodedMsg = encodeURIComponent(message)

        await axios.get(`https://api.telegram.org/bot${TOKEN}/sendMessage?chat_id=${chatIds[i]}&&text=${encodedMsg}`);
      }

      await jobRepository.updateJob({
        status: "COMPLETED",
        reason: chatIdsLength
      }, jobId);

      await userRepository.createUserMessageRecord({
        date: new Date().toISOString(),
        count: chatIdsLength
      });

    } catch (err) {
      await jobRepository.updateJob(
        {
          status: "FAILED",
          reason: err.message,
        },
        jobId
      );
      next(err);
    }
  },
};