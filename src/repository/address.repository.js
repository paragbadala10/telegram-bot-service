const Address = require("../models/Address");

module.exports = {
  saveAddress: async (data) => {
    return await Address.create(data);
  },

  updateAddress: async (data, chatId) => {
    return await Address.findOneAndUpdate({ chatId: chatId }, data, { new: true });
  },

  deleteAddress: async (id) => {
    return await Address.findOneAndDelete({_id: id});
  },

  getAddressByChatId: async (chatId) => {
    return await Address.find({chatId: chatId});
  }

};
