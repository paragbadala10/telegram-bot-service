const Job = require("../models/Job")

module.exports = {

    createJob: async (job) => {
        return await Job.create(job);
    },

    updateJob: async (job, jobId) => {
        return await Job.findOneAndUpdate({_id: jobId}, job, {new: true});
    },

    getJobById: async (jobId) => {  
        return await Job.findById(jobId);
    },

    deleteJobById: async (jobId) => {
        return await Job.findByIdAndDelete(jobId);
    }

}