const moment = require("moment");
const User = require("../models/User");
const UserMessage = require("../models/UserMessage");

module.exports = {
  createUser: async (data) => {
    return await User.create(data);
  },

  updateUser: async (data) => {
    let find = await User.find({ chatId: data.chatId });
    console.log(find)
    if (find.length) {
      return await User.findOneAndUpdate({ chatId: data.chatId }, data, { new: true });
    } else {
      let user = new User({
        ...data
      })
      return await user.save()
    }
  },

  deleteUser: async (userId) => {
    return await User.findByIdAndDelete(userId);
  },

  getUserById: async (userId) => {
    return await User.findById(userId);
  },

  getUserByChatId: async (chatId) => {
    return await User.find({ chatId: chatId });
  },

  getChatIdsByLastInteractionLessThan30Days: async () => {
    return await User.find({
      last_interaction_with_bot: {
        $gte: moment().add(-30, "days")
      }
    });
  },


  createUserMessageRecord: async (data) => {
    return await UserMessage.create(data);
  },

  getUserMessageRecord: async () => {
    return await UserMessage.find();
  },

  getChatIdsByLastInteractionMoreThan30Days: async () => {
    return await User.find({
      last_interaction_with_bot: {
        $lte: moment().add(-30, "days")
      }
    });
  }

};
