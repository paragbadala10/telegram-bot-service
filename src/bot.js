const { Telegraf, Markup } = require("telegraf");
const TOKEN = "667508284:AAGelU2rTNn3jbRcL6_Ednr_QhiHqf2S8r4";
const bot = new Telegraf(TOKEN);
const axios = require('axios');

const userChatDetails = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      let { data } = await axios.get(`https://api.telegram.org/bot${TOKEN}/getUpdates`)
      console.log(data)
      let payload
      if (data.result.length > 0) {
        let chatDetails = data.result[0].message
        payload = {
          "chatId": chatDetails.chat.id,
          "firstName": chatDetails.chat.first_name,
          "lastName": '',
          "last_interaction_with_bot": new Date(new Date - 1.5 * 60 * 60 * 1000).toLocaleString()
        }
      }

      if (data.result.length) {
        await axios.put('https://api-bot.fortheloveofbreadbakery.com/telegram-bot-service/api/v1/user', payload)
        resolve(payload)
      }
    } catch (e) {
      console.log(e)
    }
  })
  // do something with myJson
}

const web_link = "https://d2v5ks61ao4xkk.cloudfront.net";
bot.on('new_chat_members', (ctx) => {
  ctx.telegram.sendMessage(ctx.message.chat.id, `Hello ${ctx.state.role}`)
})

bot.start(async (ctx) => {
  let user = await userChatDetails()
  console.log(user)
  let text = `<b>Hi ${user.firstName}\nLet's get started👇</b> \n \nPlease tap the button below to order your perfect lunch`
  return ctx.reply(text, {
    parse_mode: 'HTML',
    ...Markup.inlineKeyboard([
      Markup.button.webApp("Order Bakery", `${web_link}?chatId=${user.chatId}&name=${user.firstName}`),
    ]),
    // ...Markup.keyboard([
    //   [{ text: "New Products", web_app: web_link + '?productType=new-arrival' },
    //   { text: "Discounted Products", web_app: web_link + '?productType=offered-product' }],
    //   [{ text: "Best Seller Products", web_app: web_link + '?productType=best-seller' },
    //   { text: "All Products", web_app: web_link + '?productType=all' }]
    // ]).resize(),
    // reply_markup: {
    //   keyboard: [
    //     [{ text: "Phone Number", request_contact: true }],
    //     [{ text: "Location", request_location: true}]
    //     ],
    //     resize_keyboard: true,
    //     one_time_keyboard: true
    // },
  })
}
);

bot.command('admin',async (ctx) => {
  let text = `<b>Hello Admin </b>\n\nPlease tap the button below to send remainders!`;

  let { data } = await axios.get(`https://api.telegram.org/bot${TOKEN}/getUpdates`)
  if (data.result.length > 0) {
    let chatId = data.result[0].message.chat.id
    if (chatId !== 224620384) {
      return
    }
  }

  return ctx.reply(text, {
    parse_mode: 'HTML',
    ...Markup.inlineKeyboard([
      Markup.button.webApp("View Admin", 'https://d2v5ks61ao4xkk.cloudfront.net/admin'),
    ]),

  })
});

bot.hears("Order Bakery", ctx => {
  console.log(ctx.message)
  userChatDetails()
})

// export default bot

module.exports = bot;

// bot.launch();
